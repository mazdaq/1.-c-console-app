-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 19, 2019 at 05:29 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `detail`) VALUES
(42, 'Data Deleted --- Time:  02/19/2019 10:28:35 PM\n\nData Respective to specified ID has been Deleted'),
(41, 'Data Deleted --- Time:  02/19/2019 10:28:23 PM\n\nData Respective to specified ID has been Deleted'),
(40, 'Data Feteched --- Time:  02/19/2019 10:28:15 PM\n\n'),
(39, 'Data Inserted --- Time:  02/19/2019 10:28:10 PM\n\nName:	 hadi khan\nEmail:	 hadi@gmail.com\nPhone:	34343'),
(37, 'Data Feteched --- Time:  02/13/2019 5:14:46 PM\n\n'),
(38, 'Data Inserted --- Time:  02/13/2019 5:28:58 PM\n\nName:	 a a\nEmail:	 a\nPhone:	a'),
(36, 'Data Deleted --- Time:  02/13/2019 5:10:47 PM\n\nData Respective to specified ID has been Deleted'),
(31, 'Data Inserted --- Time:  02/13/2019 5:03:53 PM\n\nName:	 new  data\nEmail:	 new@datama.com\nPhone:	90'),
(35, 'Data Inserted --- Time:  02/13/2019 5:10:03 PM\n\nName:	 dsfs 45\nEmail:	 vc\nPhone:	f4'),
(30, 'Data Updated --- Time:  02/13/2019 4:54:13 PM\n\nName:	 m sd\nEmail:	 dfs\nPhone:	df'),
(34, 'Data Updated --- Time:  02/13/2019 5:09:25 PM\n\nName:	 mazdaq shahzad\nEmail:	 mazdash@g..com\nPhone:	434435'),
(29, 'Data Deleted --- Time:  02/13/2019 4:53:46 PM\n\nData Respective to specified ID has been Deleted'),
(32, 'Data Inserted --- Time:  02/13/2019 5:06:44 PM\n\nName:	 Mazdaq Shahzad\nEmail:	 mazdaqshahzad@outlook.com\nPhone:	6007'),
(33, 'Data Inserted --- Time:  02/13/2019 5:06:57 PM\n\nName:	 Abdul Ahad\nEmail:	 abdulahad@gmail.com\nPhone:	435453');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `phone`) VALUES
(27, 'mazdaq', 'shahzad', 'mazdash@g..com', '434435'),
(31, 'hadi', 'khan', 'hadi@gmail.com', '34343');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
