﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Task1
{
    class Program
    {

        int option;
      
        public static void Main(string[] args)
        {
            //Initialzing objects
            Program obj = new Program();
           

            obj.MainMenu(); //Calling Main Menu Method
            Console.ReadLine();
        }

        //Method to Display Menu for choosing CRUD operations
       public void MainMenu()
        {
            //for calling CRUD operation methods from Repository
            Repository repo = new Repository();

            Console.WriteLine("Main Menu\n\n1.Insert New Record\n2.Update Exisiting Record\n3.Delete Record\n4.Show Current Record\n5.Show Log\n");
            option = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            //Switch
            switch (option)
            {
                case 1:
                    {
                        repo.insert();
                        break;
                    }

                case 2:
                    {
                        repo.update();
                        break;
                    }


                case 3:
                    {
                        repo.delete();
                        break;
                    }


                case 4:
                    {
                        Console.WriteLine("\t\t*Show Current Record Menu*\n\n");
                        repo.showRecord();
                        break;
                    }


                case 5:
                    {
                        Console.WriteLine("\t\t*Show Log Menu*\n");
                        repo.Log();
                        break;
                    }

                default:
                    {
                        Console.WriteLine("Invalid Choice");
                        break;
                    }

            }//end of switch

        }



    }
}
