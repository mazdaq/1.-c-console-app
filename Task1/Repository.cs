﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace Task1
{
    public class Repository
    {
        //Variables to Get Information from User
        string firstName, lastName, email, phone;

        Program p = new Program(); //Creating Object to Call Main Menu Method from Program.CS

        MySqlDataReader reader; // Data Reader for Reading Record

        MySqlCommand command; //For Creating and Executing Commands

        //Data Access Layer : Contain Open Connection & Close Connection Method
        DataLayer dal = new DataLayer();

        //Inserts Record Method
        public void insert()
        {
            Console.WriteLine("\t\t*Insert Record Menu*\n\n");

            //Prompt User for Basic info
            Console.WriteLine("First Name:  ");
            firstName = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\nLast Name:  ");
            lastName = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\nEmail:  ");
            email = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\nPhone:  ");
            phone = Convert.ToString(Console.ReadLine());

            //Connection Open
            dal.CreateConnection();
            
            command = dal.connection.CreateCommand(); //Create command before executing it
            
            //insert query
            command.CommandText = "insert into user(firstname,lastname,email,phone)values('" + firstName + "','" + lastName + "','" + email + "','" + phone + "')";
            command.ExecuteNonQuery();
            Console.Clear();


            //Show Insert Query Log on Console
            string log_insert;
            string timeStamp = DateTime.Now.ToString();
            log_insert = "Data Inserted --- Time:  " + timeStamp +"\n\nName:\t "+firstName+" "+lastName+"\nEmail:\t "+email+"\nPhone:\t"+phone;
            Console.WriteLine(log_insert);
            
            //And Also Inserts Insert Query Log into a Seperate Database
            command = dal.connection.CreateCommand();
            command.CommandText = "insert into log(detail)values('"+log_insert+"')";
            command.ExecuteNonQuery();

            //Connection Closed
            dal.CloseConnection();

            Console.WriteLine("\n\n---------------------------------\n\n");

            p.MainMenu(); //Call Main Method from Program.CS
           
        }


        //Update Record Method
        public void update()
        {
            int id;
            Console.WriteLine("\t\t*Update Record Menu*\n\n");

            //Opening connection - Creating Command
            dal.CreateConnection();
            command = dal.connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            //Display Existing Record before Updating it. 
            command.CommandText = "select * from user";
            reader = command.ExecuteReader();

            //heading for data
            string str = "[ID]\t[First Name]\t[Last Name]\t[Email]\t\t\t[Phone]\n-----------------------------------------------------------------------\n";

            
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    str += Convert.ToString(reader.GetInt32(0)) + "\t" + reader.GetString(1) + "\t\t" + reader.GetString(2) + "\t\t" + reader.GetString(3) + "\t\t" + reader.GetString(4) + "\t" + Environment.NewLine;
                }
            }

            Console.WriteLine(str); //Display Existing Record.

            //Select an ID of recrod to update
            Console.WriteLine("\n\nEnter ID of Record you want to Update: ");
            id = Convert.ToInt32(Console.ReadLine());

            //New Record for Updating User info
            Console.WriteLine("First Name:  ");
            firstName = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\nLast Name:  ");
            lastName = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\nEmail:  ");
            email = Convert.ToString(Console.ReadLine());

            Console.WriteLine("\nPhone:  ");
            phone = Convert.ToString(Console.ReadLine());

            //Open Connection and create command
            dal.CreateConnection();
            command = dal.connection.CreateCommand();
            
            //Update Query
            command.CommandText = "update user set firstname='"+firstName+ "', lastname='" + lastName + "' , email='" + email + "' , phone='" + phone + "' where id='"+id+"' ";
            command.ExecuteNonQuery();
            Console.Clear();

            //Show Update Query log on console
            string log_update;
            string timeStamp = DateTime.Now.ToString();
            log_update = "Data Updated --- Time:  " + timeStamp + "\n\nName:\t " + firstName + " " + lastName + "\nEmail:\t " + email + "\nPhone:\t" + phone;
            Console.WriteLine(log_update);

            //Insert Log detail into a seperate database
            command = dal.connection.CreateCommand();
            command.CommandText = "insert into log(detail)values('" + log_update + "')";
            command.ExecuteNonQuery();
            
            //Close Connection
            dal.CloseConnection();
            Console.WriteLine("\n\n---------------------------------\n\n");

            p.MainMenu(); //call Main Menu from Program.CS
        }
        

        //Delete Record Method
        public void delete()
        {
            int id;
            Console.WriteLine("Delete Record Menu\n\n");

            //Open Connection and Create Command
            dal.CreateConnection();
            command = dal.connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            //Display Existing Users
            command.CommandText = "select * from user";
            reader = command.ExecuteReader();

            //String to display user list
            string str = "[ID]\t[First Name]\t[Last Name]\t[Email]\t\t\t[Phone]\n-----------------------------------------------------------------------\n";

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    str += Convert.ToString(reader.GetInt32(0)) + "\t" + reader.GetString(1) + "\t\t" + reader.GetString(2) + "\t\t" + reader.GetString(3) + "\t\t" + reader.GetString(4) + "\t" + Environment.NewLine;
                }
            }

            Console.WriteLine(str);

            //Promoting User to select ID of record to Delete
            Console.WriteLine("Enter ID of Record you want to Delete: ");
            id = Convert.ToInt32(Console.ReadLine());
            
            //Create connection 
            dal.CreateConnection();
            command = dal.connection.CreateCommand();

            //Delete Query
            command.CommandText = "delete from user where id='" + id + "' ";
            command.ExecuteNonQuery();
            Console.Clear();

            //Show Delete Query log to Console alongside Timepstamp
            string log_delete;
            string timeStamp = DateTime.Now.ToString();
            log_delete = "Data Deleted --- Time:  " + timeStamp + "\n\nData Respective to specified ID has been Deleted";
            Console.WriteLine(log_delete);

            //Insert Delete query log into a seperate database table
            command = dal.connection.CreateCommand();
            command.CommandText = "insert into log(detail)values('" + log_delete + "')";
            command.ExecuteNonQuery();

            dal.CloseConnection();
            Console.WriteLine("\n\n---------------------------------\n\n");

            p.MainMenu();//call Main Menu from Program.CS
        }


        //Show Record Method
        public void showRecord()
        {

            //Open Connection and Create Command
            dal.CreateConnection();
            command = dal.connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            //Select User Query
            command.CommandText = "select * from user";
            reader = command.ExecuteReader();

            string str = "[ID]\t[First Name]\t[Last Name]\t[Email]\t\t\t[Phone]\n-----------------------------------------------------------------------\n";

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    str += Convert.ToString(reader.GetInt32(0)) + "\t" + reader.GetString(1) + "\t\t" + reader.GetString(2) + "\t\t" + reader.GetString(3) + "\t\t" + reader.GetString(4) + "\n" + Environment.NewLine;
                }
            }

            dal.CloseConnection(); //close connection()

            //Display select query log on console 
            string log_show;
            string timeStamp = DateTime.Now.ToString();
            log_show = "Data Feteched --- Time:  " + timeStamp +"\n\n";
            Console.WriteLine(log_show);

            //Create Connection
            dal.CreateConnection();
            command = dal.connection.CreateCommand();

            //saves a copy of log into database table
            command.CommandText = "insert into log(detail)values('" + log_show + "')";
            command.ExecuteNonQuery();
            Console.WriteLine(str);

            dal.CloseConnection(); //Close Connection()

            Console.WriteLine("\n\n---------------------------------\n\n");

            p.MainMenu(); //call Main Menu from Program.CS
        }


        //To Display Log Record of All Queries
        public void Log()
        {
            //Open Connection and Create Command
            dal.CreateConnection();
            command = dal.connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            //Select Query to display Log
            command.CommandText = "select * from log";
            reader = command.ExecuteReader();

            string str = "";

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    str += Convert.ToString("\n---------------------------------------------\n\n"+reader.GetString(1)) + "\n";
                }
            }

            Console.WriteLine(str); //Display Log

            dal.CloseConnection(); //Close Connection

            Console.WriteLine("\n\n\n---------------------------------------------\n");

            p.MainMenu(); //Call Main Menu from Program.CS
           
        }

    }
}
