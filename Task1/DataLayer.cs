﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Task1
{
    public class DataLayer
    {
        public MySqlConnection connection;
       

        //Contains Connection String - Opens Connection to Dataabse
        public void CreateConnection()
        {
            try
            {
                //Connection String
                string ConnectionString = "server=localhost;port=3306;user=root;password=root2;database=myapp;SslMode=none;";

                //Passing Connection String to MysqlConnection
                connection = new MySqlConnection(ConnectionString);

                //Open Connection
                connection.Open();
            }
            catch (MySqlException ex)
            {
                //Catch exception if there is an eror opening connection
                Console.WriteLine("Error is: " + ex.Message.ToString());
            }
        }

        //Close Connections
        public void CloseConnection()
        {
            connection.Close(); // Connection Closed
        }


    }
}
